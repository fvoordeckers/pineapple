# Pineapple

## Hosted Example

[https://fvoordeckers.bitbucket.io/pineapple/](https://fvoordeckers.bitbucket.io/pineapple/)

## Local Setup

1. clone repo
2. run `yarn` to install all the node dependencies
3. run `gulp` to build the assets
4. Open `index.html` in your browser
