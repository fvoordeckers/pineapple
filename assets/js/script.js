$(() => {
    'use strict';

    // scroll hijack
    const scroller = new Scroller();
    scroller.init()
        .then(() => {
            $('body').addClass('loaded');
        });

    // menu 
    const menu = new Menu();
    menu.init();

});
