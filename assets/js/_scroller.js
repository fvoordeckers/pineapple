class Scroller {

    constructor() {
        this.navPrev = $('.slider-nav__prev');
        this.navNext = $('.slider-nav__next');
    }

    init() {
        // initialize scroll hijacking
        return new Promise( resolve => {
            $.scrollify({
                section: '.slides__slide',
                interstitialSection: '.main-footer',
                standardScrollElements: '.main-footer',
                updateHash: false,
                before: () => {
                    this.updatePageNavigation();
                    this.updateArrowNavigation();
                },
                afterRender:() => {
                    this.updateArrowNavigation();
                    this.updatePageNavigation();

                    resolve();
                }
            });
            
            this.handleNavigation();
        });
    }

    handleNavigation() {
        // handle slider navigation
        $('.slider-nav__prev').click(() => {
            $.scrollify.previous();
        });

        $('.slider-nav__next').click(() => {
            $.scrollify.next();
        });

        // handle slider paging
        $('.slider-paging__link a').click((ev) => {
            ev.preventDefault();
            const href = $(ev.target).attr('href');
            $.scrollify.move(href);
        });
    }

    updateArrowNavigation() {
        if ($.scrollify.currentIndex() > 0) {
            this.navPrev.removeClass('hidden');
        } else {
            this.navPrev.addClass('hidden');
        }

        if ($.scrollify.currentIndex() !== $('.slides__slide').length - 1) { // index is 0-based
            this.navNext.removeClass('hidden');
        } else {
            this.navNext.addClass('hidden');
        }
    }

    updatePageNavigation() {
        const section = $.scrollify.current();
        const pageLinkClass = 'slider-paging__link';
        const activeClass = `${pageLinkClass}--active`;
        const linkToActivate = $(`.${pageLinkClass} a[href$="#${section.attr('data-slide')}"]`).parent();

        $(`.${pageLinkClass}`).removeClass(activeClass);
        linkToActivate.addClass(activeClass);
    }
}

/* exported Scroller */
