/* eslint-disable strict */
const gulp = require('gulp');
const eslint = require('gulp-eslint');
const sasslint = require('gulp-sass-lint');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const concat = require('gulp-concat');

const { task, watch, dest, src, parallel, series } = gulp;

const files = {
    styles: 'assets/scss/**/*',
    scripts: 'assets/js/**/*'
};

/**
 * Process all the style files:
 *  1. Convert SASS to CSS
 *  2. Autoprefix vender specific styling
 *  3. Prefix & Minify the css
 *  4. Write to build/css folder
 */
task('styles', () => {
    return src(files.styles)
        .pipe(sasslint())
        .pipe(sasslint.format())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            autoprefixer,
            cssnano()
        ]))
        .pipe(concat('styles.min.css'))
        .pipe(dest('build/css'));
});

/**
 * Process all the script files
 *  1. Run ESLint and print out result
 *  2. transpile ES6 with Babel
 *  3. Minify and uglify the transpiled scripts
 *  4. Write to build/js
 */
task('scripts', () => {
    return src(files.scripts)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(babel({
			presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(dest('build/js'));
});

/**
 * Default gulp task
 */
task('default', parallel('styles', 'scripts'));

/**
 * File watchers for all the files in the extension
 * Ech file change will trigger a new gulp task
 */
task('watch', series( 'default', () => {
    watch(files.styles, series('styles'));
    watch(files.scripts, series('scripts'));
}));
